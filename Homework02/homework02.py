#!/usr/bin/python3
from PIL import Image
import matplotlib.pyplot as pyplot
from sklearn import datasets
from sklearn import svm
import numpy
import random
from time import time
import seaborn

DEBUG = False

def randomize_dataset(samples, labels):
    t0 = time()
    permutation = numpy.random.permutation(samples.shape[0])
    print("dataset shuffled in {:.3f} s.".format(time() - t0))
    return samples[permutation], labels[permutation]

def split_dataset(samples, labels):
    t0 = time()
    training_size = int(labels.size / 2)
    validation_size = int(labels.size / 5)
    test_size = int(labels.size / 3)

    if DEBUG:
        print("dataset size         {:3d} samples".format(labels.size))
        print("training set size    {:3d} samples".format(training_size))
        print("validation set size  {:3d} samples".format(validation_size))
        print("test set size        {:3d} samples".format(test_size))

    print("dataset split in {:.3f} s.".format(time() - t0))

    if DEBUG:
        pyplot.figure()
        colors = "bry"
        pyplot.scatter(samples[: training_size, 0], samples[: training_size, 1], \
            c = "b", label = 'training', \
            cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 25)
        pyplot.scatter(samples[training_size : training_size + validation_size, 0], samples[training_size : training_size + validation_size, 1], \
            c = "r", label = 'validation', \
            cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 25)
        pyplot.scatter(samples[-test_size:, 0], samples[-test_size:, 1], \
            c = "y", label = 'test', \
            cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 25)
        pyplot.legend()

    return samples[: training_size, :], labels[: training_size], \
        samples[training_size : training_size + validation_size, :], labels[training_size : training_size + validation_size], \
        samples[-test_size:, :], labels[-test_size:]

def plot_data(X, labels, clf):
    h = .01 # step size in the mesh
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = numpy.meshgrid(numpy.arange(x_min, x_max, h), numpy.arange(y_min, y_max, h))
    Z = clf.predict(numpy.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    cs = pyplot.contourf(xx, yy, Z, cmap = pyplot.cm.Pastel1)
    pyplot.axis("tight")

    # Print classes
    colors = "bry"
    pyplot.scatter(X[:50, 0], X[:50, 1], c = "b", label = labels[0], \
        cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 15)
    pyplot.scatter(X[50:100, 0], X[50:100, 1], c = "r", label = labels[1], \
        cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 15)
    pyplot.scatter(X[100:, 0], X[100:, 1], c = "y", label = labels[2], \
        cmap = pyplot.cm.Pastel2, edgecolor = "k", s = 15)

def standardize_data(data):
    t0 = time()
    data_mean   = data.mean(axis = 0)
    data_std    = data.std(axis = 0)
    data_scaled = (data - data_mean) / data_std
    print("data standardized in {:.3f} s.".format(time() - t0))
    return data_scaled, data_mean, data_std

def compute_svm(X_train, y_train, X_valid, y_valid, X_test, y_test, labels, kernel_type):
    accuracy = []

    pyplot.figure()
    pyplot.suptitle("SVM {} kernel".format(kernel_type))

    print("\n{} kernel results".format(kernel_type))
    for i in range(-3, 4):
        clf = svm.SVC(kernel = kernel_type, C = 10 ** i, gamma = "auto")
        clf.fit(X_train, y_train)
        clf_predictions = clf.predict(X_test)
        score = clf.score(X_valid, y_valid) * 100
        pyplot.subplot(2, 4, i + 4)
        plot_data(X_scaled, labels, clf)
        pyplot.title("c = {}".format(10 ** i))
        accuracy.append(score)
        print("validation accuracy using C = {}: {:.2f}%".format(10 ** i, score))

    max_accuracy, index_max_accuracy = max(accuracy), accuracy.index(max(accuracy))
    c = 10 ** (index_max_accuracy - 3)

    pyplot.figure()
    pyplot.plot([10 ** i for i in range(-3, 4)], accuracy, ls = "solid")
    pyplot.xscale("log")
    pyplot.grid()
    pyplot.title("SVM {} kernel - accuracy".format(kernel_type))

    print("max accuracy {:.2f}% achieved using C = {}".format(max_accuracy, c))
    clf = svm.SVC(kernel = kernel_type, C = c, gamma = "auto")
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test) * 100
    print("test accuracy using C = {}: {:.2f}%".format(c, score))

def compute_grid_search(X_train, y_train, X_valid, y_valid, X_test, y_test, labels):
    c_vals = [10 ** i for i in range (-2, 3)]
    gamma_vals = [10 ** i for i in range (-3, 3)]
    scores = numpy.empty([len(gamma_vals), len(c_vals)]).astype(float)

    print("\nrbf kernel results")
    for i, gamma in enumerate(gamma_vals):
        for j, c in enumerate(c_vals):
            clf = svm.SVC(kernel = "rbf", C = c, gamma = gamma)
            clf.fit(X_train, y_train)
            score = clf.score(X_valid, y_valid)
            scores[i, j] = score
            print("validation accuracy using C = {} and gamma = {}: {:.2f}%".format(c, gamma, score * 100))

    pyplot.figure()
    seaborn.heatmap(scores, annot = True, xticklabels = c_vals, yticklabels = gamma_vals)
    pyplot.title("rbf kernel results")
    pyplot.xlabel("c")
    pyplot.ylabel("gamma")

    max_accuracy = numpy.max(scores, axis = None)
    index_max_accuracy = numpy.unravel_index(numpy.argmax(scores, axis = None), scores.shape)
    gamma_max = numpy.float_power(10, index_max_accuracy[0] - 3)
    c_max = numpy.float_power(10, index_max_accuracy[1] - 2)
    print("max accuracy {:.2f}% achieved using C = {} and gamma = {}".format(max_accuracy * 100, c_max, gamma_max))
    clf = svm.SVC(kernel = "rbf", C = c_max, gamma = gamma_max)
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test) * 100
    print("test accuracy using C = {} and gamma = {}: {:.2f}%".format(c_max, gamma_max, score))

    pyplot.figure()
    pyplot.suptitle('rbf kernel using C = {}, gamma = {}'.format(c_max, gamma_max))
    plot_data(X_scaled, labels, clf)

def compute_k_fold(X_train, y_train, X_test, y_test, labels):
    folds = 5
    c_vals = [10 ** i for i in range (-2, 3)]
    gamma_vals = [10 ** i for i in range (-3, 3)]
    scores = numpy.empty([len(gamma_vals), len(c_vals)]).astype(float)
    fold_size = int(X_train.shape[0] / folds)
    X_train_folds = numpy.empty([folds, fold_size * (folds - 1), 2]).astype(float)
    y_train_folds = numpy.empty([folds, fold_size * (folds - 1)]).astype(float)
    X_valid_folds = numpy.empty([folds, fold_size, 2]).astype(float)
    y_valid_folds = numpy.empty([folds, fold_size]).astype(float)

    for i in range(folds):
        X_train_folds[i] = numpy.concatenate((X_train[ : i * fold_size], X_train[(i + 1) * fold_size : ]), axis = 0)
        y_train_folds[i] = numpy.concatenate((y_train[ : i * fold_size], y_train[(i + 1) * fold_size : ]), axis = 0)
        X_valid_folds[i] = X_train[i * fold_size : (i + 1) * fold_size]
        y_valid_folds[i] = y_train[i * fold_size : (i + 1) * fold_size]

    print("\nk-fold results")
    for i, gamma in enumerate(gamma_vals):
        for j, c in enumerate(c_vals):
            scores[i, j] = 0
            for k in range(0, folds):
                clf = svm.SVC(kernel = "rbf", C = c, gamma = gamma)
                clf.fit(X_train_folds[k], y_train_folds[k])
                score = clf.score(X_valid_folds[k], y_valid_folds[k])
                scores[i, j] += score

            scores[i, j] /= folds
            print("average validation accuracy using C = {} and gamma = {}: {:.2f}%".format(c, gamma, scores[i, j] * 100))

    pyplot.figure()
    seaborn.heatmap(scores, annot = True, xticklabels = c_vals, yticklabels = gamma_vals)
    pyplot.title("rbf kernel results (k-fold avg)")
    pyplot.xlabel("c")
    pyplot.ylabel("gamma")

    # avgs = scores.mean(axis = 2)
    max_accuracy = numpy.max(scores, axis = None)
    index_max_accuracy = numpy.unravel_index(numpy.argmax(scores, axis = None), scores.shape)
    gamma_max = numpy.float_power(10, index_max_accuracy[0] - 3)
    c_max = numpy.float_power(10, index_max_accuracy[1] - 2)
    print("max accuracy {:.2f}% achieved using C = {} and gamma = {}".format(max_accuracy * 100, c_max, gamma_max))
    clf = svm.SVC(kernel = "rbf", C = c_max, gamma = gamma_max)
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test) * 100
    print("test accuracy using C = {} and gamma = {}: {:.2f}%".format(c_max, gamma_max, score))

    pyplot.figure()
    pyplot.suptitle('model built using k-fold: c = {} gamma = {} '.format(c_max, gamma_max))
    plot_data(X_scaled, labels, clf)

if __name__ == "__main__":
    tt0 = time()
    random.seed(time())

    iris = datasets.load_iris()
    X_orig = iris.data[:, :2]
    y_orig = iris.target
    labels = iris.target_names

    X_scaled, X_mean, X_std = standardize_data(X_orig)
    X_, y_ = randomize_dataset(X_scaled, y_orig)
    X_train, y_train, X_valid, y_valid, X_test, y_test = split_dataset(X_, y_)
    compute_svm(X_train, y_train, X_valid, y_valid, X_test, y_test, labels, "linear")
    compute_svm(X_train, y_train, X_valid, y_valid, X_test, y_test, labels, "rbf")
    compute_grid_search(X_train, y_train, X_valid, y_valid, X_test, y_test, labels)

    # Merge train and validation datasets
    X_train = numpy.concatenate((X_train, X_valid), axis = 0)
    y_train = numpy.concatenate((y_train, y_valid), axis = 0)
    compute_k_fold(X_train, y_train, X_test, y_test, labels)

    print("\ntotal time: {:.3f} s".format(time() - tt0))
    pyplot.show()
