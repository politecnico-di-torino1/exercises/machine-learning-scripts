\documentclass{report}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[a4paper, top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{\textbf{Machine Learning \& Artifical Intelligence}}
\rhead{Homework 03}
\lfoot{Enrico Franco}
\rfoot{Politecnico di Torino}
\usepackage{graphicx}					% Images
\usepackage{subcaption}
\usepackage{booktabs}					% Tables
\usepackage{multirow}
\usepackage[hidelinks]{hyperref}		% Links
\author{Enrico Franco}
\title{Machine Learning \& Artifical Intelligence \\ Homework 03}

\begin{document}
\section*{Traditional Neural Network}
\emph{Traditional Neural Network} reaches an accuracy of 26\% at the end of the twentieth epoch. Considering the number of different classes composing the dataset, this value may be considered a sufficient result. Indeed, considering that the dataset is made up of 100 different classes, a $(25 \div 30) \%$ chance of predicting the correct label seems acceptable with respect to 1\% probability to guess the correct label.

Figure~\ref{img:traditional_results} shows the result curves:
\begin{itemize}
\item Accuracy at the end of the training is not yet stable and seems increasing, so it is possible that continuing the training for some more epochs, we would to a slightly better result;
\item Loss at the end of the training is still decreasing, confirming the fact that the network may improve its performance with more iterations on the dataset.
\end{itemize}

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{01_traditional/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{01_traditional/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{Traditional Neural Network -- Results}
\label{img:traditional_results}
\end{figure}

\section*{Convolutional Neural Network}
\emph{Convolutional Neural Network} in its basic version reaches an accuracy of 29\%, which is not so better than the previous result achieved using a \emph{Traditional Neural Network}. There is a small improvement probabily because \emph{Convolutional Neural Networks} can learn hierarchical features and, consequently, establish which features are useful for the classification and how to compute them.

Figure~\ref{img:basic_cnn} shows the result curves, which are pretty similar to the previous ones. The main difference is that, in this case, accuracy is still growing, and loss function decreases more rapidly, as it is possible to notice from figure~\ref{img:nn_vs_cnn}.

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{02_cnn_basic/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{02_cnn_basic/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{Convolutional Neural Network -- Results}
\label{img:basic_cnn}
\end{figure}

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{02_cnn_basic/01v02_accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{02_cnn_basic/01v02_loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{Traditional Neural Network vs.\@ Convolutional Neural Network}
\label{img:nn_vs_cnn}
\end{figure}

\section*{Convolutional Neural Network -- Wider layers}
Figure~\ref{img:cnn_wider} shows the curves of different neural networks which make use of convolutional layers of different sizes. It is possible to notice that increasing the size of the filters, there is a continuous increase in accuracy values, due to the fact that increasing the number of kernels for each convolutional layer, it is possible to capture and learn more features from the sample data. Furthermore, it is possible to notice that the loss function rapidly reaches low values and consequently the accuracy does not increase more.

Increasing the number of feature maps does not guarantee better performance, because it is possible to overfit the network. Although, in our case for what has been said above there are advantages.

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{03_cnn_layers/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{03_cnn_layers/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{CNN wider layers -- Results}
\label{img:cnn_wider}
\end{figure}

\section*{Convolutional Neural Network -- Improvements}
Figure~\ref{img:cnn_vs_improvements} shows the different behaviours of a CNN which does not use any regularization technique and some CNNs which use some.

\paragraph{Batch Normalization}
\emph{Batch Normalization} should help gradients propagation in deeper networks by reducing internal covariate shift between successive layers. In this way, batch normalization may permit the training of deeper networks, which might not converge otherwise. 

Generally speaking, it speeds up training of deep networks by a significant factor. In our case we can observe how compared to the case in which the Batch Normalization was not present the loss function decreases much less quickly allowing the network to not converge after only 10 epochs and then helping it to reach an higher level of accuracy.

\paragraph{Fully Connected Layer}
Increasing the size of the \emph{Fully Connected Layer}, apparently, does not affect the accuracy and loss curves, which have a similar behaviour compared to a smaller fully connected layer.

\paragraph{Dropout}
\emph{Dropout} forces the network to learn more robust features, possibly different on each epoch. Training is therefore slower, in fact, after 20 epochs the loss is higher with respect to other networks without dropout. However, training time accuracy converges faster, with a higher value.

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{04_cnn_modifications/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{04_cnn_modifications/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{CNN improvements -- Results}
\end{figure}

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{04_cnn_modifications/improvements_vs_basic_accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{04_cnn_modifications/improvements_vs_basic_loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{128/128/128/256 CNN vs.\@ CNN improvements}
\label{img:cnn_vs_improvements}
\end{figure}

\section*{Data augmentation}
In order to train the large amount of parameters in a network, it is needed to provide a huge amount of samples during training phase, which may not be available. If so, it is possible to make minor alterations to our existing dataset, e.g., flips, crops, translations or rotations.

Generally speaking, \emph{data augmentation} slows down the network convergence because the network is trained on noise data, but it should generate a more robust model, with better performance at training time, if the augmentation techniques do not create, or increase, irrelevant data. In fact, data augmentation may sometimes mislead if the generated data does not have any relationship with the original one. Furthermore, augmentation has a regularizing effect, i.e., too much data augmentation, combined with other forms of regularization might cause the network to underfit.

Figure~\ref{img:data_augmentation} shows the result curves, which prove these considerations. It is possible to notice that the network trained without data augmentation converges faster, but with a lower accuracy with respect to the networks trained using data augmentation. \emph{Horizontal flipping} seems working better in the first epochs with respect to \emph{Random crop}, because it converges faster and loss decreases rapidly, even if the second one seems quite far from its convergence and may lead to better performance. This behaviour might be justified if the dataset already contains flipped or mirrored images, while recognizing part of images is generally a more difficult task.

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{05_data_augmentation/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{05_data_augmentation/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{CNN with data augmentation -- Results}
\label{img:data_augmentation}
\end{figure}

\section*{ResNet18}
A lot of data are needed to train a Convolutional Neural Network, therefore a widely exploited technique is to use a big network trained on a very big dataset and train or finetune just a few layers of it.

In figure~\ref{img:resnet}, it is possible to notice that using different data augmentation techniques, the accuracy is quite high since the first epoch, reaching a stable value greater than 70\% in few epochs, with the loss curve rapidly converging to zero.

\begin{figure}[hbtp]
\centering
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{06_resnet18/accuracy.png}
	\subcaption{Accuracy}
	\end{minipage}
	\quad
	\begin{minipage}{.45\textwidth}
	\centering
	\includegraphics[scale=0.43]{06_resnet18/loss.png}
	\subcaption{Loss}
	\end{minipage}
\caption{ResNet18 -- Results}
\label{img:resnet}
\end{figure}

\end{document}
