#!/usr/bin/python3
from PIL import Image
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np
import os, sys
import random
from time import time

# Global definition
DATASET_PATH    = "dataset/"
WIDTH           = 227
HEIGHT          = 227
PCA_COMPONENTS  = 2
IMAGES_CLASS    = 100
TESTS           = 5

# Colors
COLORS          = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']

# Dictionary fields
DATA_FIELD      = "data"
LABEL_FIELD     = "label"
IMAGE_FIELD     = "image"

def read_dataset(path):
    global_counter = 0
    dataset = {}                # Dictionary
    dataset[LABEL_FIELD] = []   # List
    dataset[IMAGE_FIELD] = []   # List
    tmp_concat = []             # Array concatenation

    t0 = time()
    n_classes = 0

    # For all labels
    for directory in os.listdir(path):
        directory_path = path + directory + "/"                 # Directory path concatenation
        local_counter = 0
        # For all files
        for file in os.listdir(directory_path):
            image_path = directory_path + file                  # Image path concatenation
            image_data = np.asarray(Image.open(image_path))     # 3-D representation
                                                                # WIDTH x HEIGHT x 3 (RGB)
            x = image_data.ravel()                              # Array representation
            tmp_concat.append(x)                                # Add feature row
            dataset[LABEL_FIELD].append(directory)              # Save the label
            dataset[IMAGE_FIELD].append(file)                   # Save the filename
            local_counter += 1
            if(local_counter == IMAGES_CLASS):
                break

        n_classes += 1                                          # New class discovered
        global_counter += local_counter
        print("{:d} {} images read".format(local_counter, directory))

    print("{:d} total images read in {:.3f} s".format(global_counter, time() - t0))
    dataset[DATA_FIELD] = np.asarray(tmp_concat)
    return dataset, n_classes

def show_different_pca(dataset, img_idx, components):
    plt.figure()
    plt.title("PCA Projection")

    # Plot original image
    original_image = data[DATA_FIELD][img_idx].reshape(WIDTH, HEIGHT, 3)
    plt.subplot(1, 5, 1)
    plt.title("Original image")
    plt.imshow(original_image)

    # Scaling data
    data_scaled, data_mean, data_std = standardize_data(dataset)

    # Compute PCA and plot
    position = 2
    for n_components in components:
        t0 = time()
        pca = PCA(n_components)
        components = pca.fit_transform(data_scaled)
        print("{:d} PCA computed in {:.3f} s. Total variance: {:.2f}%".format(n_components, time() - t0, np.sum(pca.explained_variance_ratio_) * 100))
        t0 = time()
        result = pca.inverse_transform(components)
        print("Reverse {:d} PCA computed in {:.3f} s".format(n_components, time() - t0))

        pca_image = (result[img_idx] * data_std[img_idx] + data_mean[img_idx]).reshape(WIDTH, HEIGHT, 3).astype(int)
        plt.subplot(1, 5, position)
        plt.title("{:d} PC".format(n_components))
        plt.imshow(pca_image)

        position = position + 1

    # Show last 6 PCs
    n_components = 6
    t0 = time()
    pca = PCA()
    transform = pca.fit_transform(data_scaled)
    print("PCA computed in {:.3f} s.".format(time() - t0))
    result = np.dot(transform[:,-n_components:], pca.components_[-n_components:,:])
    print("Reverse {:d} PCA computed in {:.3f} s".format(n_components, time() - t0))
    pca_image = (result[img_idx] * data_std[img_idx] + data_mean[img_idx]).reshape(WIDTH, HEIGHT, 3).astype(int)
    plt.subplot(1, 5, 5)
    plt.title("Last {:d} PC".format(n_components))
    plt.imshow(pca_image)

    return pca # Full PCA

def show_scatter_plot(dataset, labels):
    n_components = 11
    data_scaled, data_mean, data_std = standardize_data(dataset)
    t0 = time()

    transform = PCA(n_components).fit_transform(data_scaled)
    print("{:d} PCA computed in {:.3f} s".format(n_components, time() - t0))

    plt.figure()
    plt.title("First and second PCs - Comparison")
    for i in range(0, classes):
        plt.scatter(transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 0], transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 1], c = COLORS[i], label = data[LABEL_FIELD][IMAGES_CLASS * i])
    plt.legend()

    plt.figure()
    plt.title("Third and fourth PCs - Comparison")
    for i in range(0, classes):
        plt.scatter(transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 2], transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 3], c = COLORS[i], label = data[LABEL_FIELD][IMAGES_CLASS * i])
    plt.legend()

    plt.figure()
    plt.title("Tenth and Eleventh PCs - Comparison")
    for i in range(0, classes):
        plt.scatter(transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 9], transform[IMAGES_CLASS * i : IMAGES_CLASS * (i+1), 10], c = COLORS[i], label = data[LABEL_FIELD][IMAGES_CLASS * i])
    plt.legend()

def show_cumulative_sum(full_pca):
    plt.figure()
    plt.title("Cumulative variance")
    plt.plot(np.cumsum(full_pca.explained_variance_ratio_))
    plt.grid()
    plt.xlabel("number of components")
    plt.ylabel("cumulative explained variance")

def standardize_data(data):
    t0 = time()
    data_mean   = data.mean(axis = 0)
    data_std    = data.std(axis = 0)
    data_scaled = (data - data_mean) / data_std
    print("Data standardized in {:.3f} s".format(time() - t0))
    return data_scaled, data_mean, data_std

if __name__ == "__main__":
    tt0 = time()
    random.seed(time())

    ### Data preparation ###
    data, classes = read_dataset(DATASET_PATH)
    images = classes * IMAGES_CLASS

    # Image selection
    img_idx = random.randint(0, images - 1)
    img_name = data[IMAGE_FIELD][img_idx]
    img_label = data[LABEL_FIELD][img_idx]
    print("I have selected image {} from {} subset".format(img_name, img_label))

    ### Principal Vomponent Visualization ###
    full_pca = show_different_pca(data[DATA_FIELD], img_idx, [2, 6, 60])
    show_scatter_plot(data[DATA_FIELD], data[LABEL_FIELD])
    show_cumulative_sum(full_pca)

    data_scaled, data_mean, data_std = standardize_data(data[DATA_FIELD])

    ### Classification ###
    from sklearn.naive_bayes import GaussianNB
    from sklearn.model_selection import train_test_split
    samples = data_scaled
    labels = data[LABEL_FIELD]
    gnb = GaussianNB()
    global_score = 0
    print("Testing classifier on original images ...")
    for i in range(0, TESTS):
        samples_training, samples_testing, labels_training, labels_testing = train_test_split(samples, labels)
        gnb.fit(samples_training, labels_training)
        score = gnb.score(samples_training, labels_training)
        global_score += score
        print("Test {:0d}: Score = {:.2f}%".format(i, score * 100))
    print("Original images. Average score = {:.2f}%".format(global_score / TESTS * 100))

    components = PCA(4).fit_transform(data_scaled)
    global_score = 0
    print("Testing classifier on first 2 PCs ...")
    for i in range(0, TESTS):
        samples_training, samples_testing, labels_training, labels_testing = train_test_split(components[:, :2], labels)
        gnb.fit(samples_training, labels_training)
        score = gnb.score(samples_training, labels_training)
        global_score += score
        print("Test {:0d}: Score = {:.2f}%".format(i, score * 100))
    print("First 2 PCs. Average score = {:.2f}%".format(global_score / TESTS * 100))

    print("Testing classifier on first 3rd-4th PCs ...")
    global_score = 0
    for i in range(0, TESTS):
        samples_training, samples_testing, labels_training, labels_testing = train_test_split(components[:, 2:3], labels)
        gnb.fit(samples_training, labels_training)
        score = gnb.score(samples_training, labels_training)
        global_score += score
        print("Test {:0d}: Score = {:.2f}%".format(i, score * 100))
    print("3rd-4th 2 PCs. Average score = {:.2f}%".format(global_score / TESTS * 100))

    print("Testing classifier on first 4 PCs ...")
    global_score = 0
    for i in range(0, TESTS):
        samples_training, samples_testing, labels_training, labels_testing = train_test_split(components, labels)
        gnb.fit(samples_training, labels_training)
        score = gnb.score(samples_training, labels_training)
        global_score += score
        print("Test {:0d}: Score = {:.2f}%".format(i, score * 100))
    print("First 4 PCs. Average score = {:.2f}%".format(global_score / TESTS * 100))

    print("Total time: {:.3f} s".format(time() - tt0))
    plt.show()
